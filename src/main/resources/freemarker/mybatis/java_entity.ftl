package ${codeFileConfig.entityPackage};

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ${tabInfos.description}实体类  - ${tabInfos.tableName}
 *
 * @author ${codeFileConfig.author} at ${codeFileConfig.createDate}
 * @version ${codeFileConfig.version}
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("${tabInfos.tableName}")
public class ${tabInfos.entityName}PO implements Serializable {

	private static final long serialVersionUID = 1L;
	
    // Fields    
	<#list tabInfos.colums as column>
 	<#if column.dataType=='NUMBER' || column.dataType=='int2' || column.dataType=='int4'>
 		<#assign type = 'Integer'>
 	<#elseif column.dataType=='DATE' || column.dataType=='date' || column.dataType=='timestamp'>
 		<#assign type = 'Date'>
 	<#elseif column.dataType=='int8'>
 		<#assign type = 'Long'>
 	<#elseif column.dataType=='float4'>
 		<#assign type = 'Float'>
 	<#elseif column.dataType=='float8'>
 		<#assign type = 'Double'>
 	<#else>
 		<#assign type = 'String'>
 	</#if>
 	/**${column.comments}**/
 	<#if column.pkIdField>
 	@TableId(value = "${column.columnName}", type = IdType.UUID)	
 	<#else>
 	@TableField("${column.columnName}")
 	</#if>
 	private ${type} ${column.fieldName};
	</#list>
}