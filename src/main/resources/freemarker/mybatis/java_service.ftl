package ${codeFileConfig.servicePackage};

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${codeFileConfig.entityPackage}.${tabInfos.entityName}PO;
import ${codeFileConfig.repositoryPackage}.${tabInfos.entityName}Mapper;

/**
 * ${tabInfos.description}业务处理类
 *
 * @author ${codeFileConfig.author} at ${codeFileConfig.createDate}
 * @version ${codeFileConfig.version}
 */
@Service
public class ${tabInfos.entityName}Service extends ServiceImpl<${tabInfos.entityName}Mapper, ${tabInfos.entityName}PO>{

}