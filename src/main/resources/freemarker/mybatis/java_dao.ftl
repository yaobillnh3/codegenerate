package ${codeFileConfig.repositoryPackage};


import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${codeFileConfig.entityPackage}.${tabInfos.entityName}PO;

/**
 * ${tabInfos.description}crud操作类
 *
 * @author ${codeFileConfig.author} at ${codeFileConfig.createDate}
 * @version ${codeFileConfig.version}
 */
@Repository
public interface ${tabInfos.entityName}Mapper extends BaseMapper<${tabInfos.entityName}PO>{

}