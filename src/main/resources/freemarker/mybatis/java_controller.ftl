package ${codeFileConfig.controllerPackage};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${codeFileConfig.entityVOPackage}.${tabInfos.entityName}VO;
import ${codeFileConfig.servicePackage}.${tabInfos.entityName}Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ${tabInfos.description}controller
 *
 * @author ${codeFileConfig.author} at ${codeFileConfig.createDate}
 * @version ${codeFileConfig.version}
 */
@RestController
@RequestMapping(value = "${codeFileConfig.requestMapping}")
@Api(description = "${tabInfos.description}")
public class ${tabInfos.entityName}Controller {

	@Autowired
	${tabInfos.entityName}Service ${tabInfos.entityName?uncap_first}Service;

	/**
	 * 分页查询${tabInfos.description}列表
	 * 
	 * @param pageable
	 *            分页对象
	 * @return
	 */
	@ApiOperation(value = "${tabInfos.description}列表")
	@GetMapping(value = "")
	public Page<${tabInfos.entityName}VO> page(Pageable pageable) {
		return null;
	}

	/**
	 * 保存/修改${tabInfos.description}
	 * 
	 * @param ${tabInfos.entityName?uncap_first}Po
	 *            ${tabInfos.description}
	 * @return
	 */
	@ApiOperation(value = "保存/修改")
	@PostMapping(value = "")
	public boolean saveOrUpdate(${tabInfos.entityName}VO ${tabInfos.entityName?uncap_first}Vo) {
		return true;
	}

	/**
	 * 根据主键ID删除记录
	 * @param pkId 主键ID
	 * @return
	 */
	@ApiOperation(value = "删除")
	@DeleteMapping(value = "/{pkId}")
	public boolean deleteById(@ApiParam(value="主键ID", required = true) @PathVariable(required = true) String pkId) {
		return true;
	}

}