package ${codeFileConfig.servicePackage};

import org.springframework.data.domain.Pageable;

import ${codeFileConfig.entityVOPackage}.InPage${tabInfos.entityName}BO;
import ${codeFileConfig.entityVOPackage}.OutPage${tabInfos.entityName}BO;

/**
 * ${tabInfos.description}业务
 * @author ${codeFileConfig.author} 
 * createDate ${codeFileConfig.createDate}
 */
public interface ${tabInfos.entityName}Service {

	public Page<OutPage${tabInfos.entityName}BO> page(Pageable pageable, InPage${tabInfos.entityName}BO inPage${tabInfos.entityName}BO);
	
	void save(InSave${tabInfos.entityName}BO inSave${tabInfos.entityName}BO);
	
	void deleteById(String id);
	
}