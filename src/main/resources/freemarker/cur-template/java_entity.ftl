package ${codeFileConfig.entityPackage};

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ${tabInfos.description}实体
 * @author ${codeFileConfig.author} 
 * createDate ${codeFileConfig.createDate}
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "${tabInfos.tableName}")
public class ${tabInfos.entityName}PO implements Serializable {

	private static final long serialVersionUID = 1L;
	
    // Fields    
	<#list tabInfos.colums as column>
 	<#if column.dataType=='NUMBER' || column.dataType=='int2' || column.dataType=='int4'>
 		<#assign type = 'Integer'>
 	<#elseif column.dataType=='DATE' || column.dataType=='date' || column.dataType=='timestamp'>
 		<#assign type = 'Date'>
 	<#elseif column.dataType=='int8'>
 		<#assign type = 'Long'>
 	<#elseif column.dataType=='float4'>
 		<#assign type = 'Float'>
 	<#elseif column.dataType=='float8'>
 		<#assign type = 'Double'>
 	<#else>
 		<#assign type = 'String'>
 	</#if>
 	/**${column.comments}**/
 	<#if column.pkIdField>
 	@Id
 	@Column(name = "${column.columnName}", nullable = false)
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "uuid")	
 	<#else>
 	@Column(name = "${column.columnName}")
 	</#if>
 	private ${type} ${column.fieldName};
	</#list>
}