package ${codeFileConfig.repositoryPackage};


import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import ${codeFileConfig.entityPackage}.${tabInfos.entityName}PO;

/**
 * ${tabInfos.description}数据库API
 * @author ${codeFileConfig.author} 
 * createDate ${codeFileConfig.createDate}
 */
@Repository
public interface ${tabInfos.entityName}Repository extends CrudRepository<${tabInfos.entityName}PO, String>, JpaSpecificationExecutor<${tabInfos.entityName}PO>{

}