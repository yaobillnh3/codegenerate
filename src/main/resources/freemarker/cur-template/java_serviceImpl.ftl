package ${codeFileConfig.servicePackage};

import org.springframework.data.domain.Pageable;

import ${codeFileConfig.entityVOPackage}.${tabInfos.entityName}BO;
import ${codeFileConfig.servicePackage}.${tabInfos.entityName}Service;
/**
 * ${tabInfos.description}业务
 * @author ${codeFileConfig.author} 
 * createDate ${codeFileConfig.createDate}
 */
public interface ${tabInfos.entityName}ServiceImpl implements ${tabInfos.entityName}Service {

	Page<${tabInfos.entityName}BO> page(Pageable pageable);
	
	void save();
	
	void deleteById(String id);
	
}