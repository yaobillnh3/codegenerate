package ${codeFileConfig.entityVOPackage};

import java.util.Date;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ${tabInfos.description}VO
 * @author ${codeFileConfig.author} 
 * createDate ${codeFileConfig.createDate}
 */
@ApiModel()
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ${tabInfos.entityName}VO implements Serializable {

	private static final long serialVersionUID = 1L;
	
    // Fields    
	<#list tabInfos.colums as column>
 	<#if column.dataType=='NUMBER' || column.dataType=='int2' || column.dataType=='int4'>
 		<#assign type = 'Integer'>
 		<#assign validExpress = '@NotNull(message="${column.comments}不能为空")'>
 	<#elseif column.dataType=='DATE' || column.dataType=='date' || column.dataType=='timestamp'>
 		<#assign type = 'Date'>
 		<#assign validExpress = '@NotNull(message = "${column.comments}不能为空")'>
 	<#elseif column.dataType=='int8'>
 		<#assign type = 'Long'>
 		<#assign validExpress = '@NotNull(message = "${column.comments}不能为空")'>
 	<#elseif column.dataType=='float4'>
 		<#assign type = 'Float'>
 		<#assign validExpress = '@NotNull(message = "${column.comments}不能为空")'>
 	<#elseif column.dataType=='float8'>
 		<#assign type = 'Double'>
 		<#assign validExpress = '@NotNull(message = "${column.comments}不能为空")'>
 	<#else>
 		<#assign type = 'String'>
 		<#assign validExpress = '@NotBlank(message = "${column.comments}不能为空")'>
 	</#if>
 	
 	/**${column.comments}**/
 	@ApiModelProperty(value = "${column.comments}")
 	<#if column.nullable=='false'>
 	${validExpress}
 	</#if>
 	private ${type} ${column.fieldName};
	</#list>
}