package com.sailing.codegenerate.database.db;

import com.sailing.codegenerate.database.utils.StringKits;
/**
 * 数据库字段信息
 * @author YW
 *
 */
public class Column {
	/**
	 * 数据库表中列列名
	 */
	public boolean pkIdField = false;
	/**
	 * 数据库表中列列名
	 */
	public String columnName;
	/**
	 * 实体字段名
	 */
	public String fieldName;
	/**
	 * 数据类型
	 */
	public String dataType;
	/**
	 * 字段长度
	 */
	public int dataLength;
	/**
	 * 是否为空
	 */
	public String nullable;
	/**
	 * 列中文注释
	 */
	public String comments;

	public Column(String columnName,String comments) {
		this.columnName = columnName;
		this.comments = comments;
	}

	public Column(String columnName, String dataType, int dataLength, String nullable, String comments) {
		super();
		this.columnName = columnName;
		this.fieldName = StringKits.underlineToHump(columnName);
		this.dataType = dataType;
		this.dataLength = dataLength;
		this.nullable = String.valueOf("Y".equals(nullable));
		this.comments = comments;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public int getDataLength() {
		return dataLength;
	}

	public void setDataLength(int dataLength) {
		this.dataLength = dataLength;
	}

	public String getNullable() {
		return nullable;
	}

	public void setNullable(String nullable) {
		this.nullable = nullable;
	}

	public String getComments() {
		if(comments == null) {
			return "";
		}
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isPkIdField() {
		return pkIdField;
	}

	public void setPkIdField(boolean pkIdField) {
		this.pkIdField = pkIdField;
	}

}
