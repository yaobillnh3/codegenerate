package com.sailing.codegenerate.database.db;

import java.util.ArrayList;
import java.util.List;
/**
 * 表信息-包含所有字段集合
 * @author YW
 *
 */
public class TabInfos {
	/**
	 * 字段集合
	 */
	List<Column> colums = new ArrayList<>();
	/**
	 * 表名
	 */
	private String tableName = "";
	/**
	 * 实体名
	 */
	private String entityName = "";
	/**
	 * 表的描述信息
	 */
	private String description = "";

	public List<Column> getColums() {
		return colums;
	}

	public void setColums(List<Column> colums) {
		this.colums = colums;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getDescription() {
		if(description == null ) {
			return "";
		}
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
