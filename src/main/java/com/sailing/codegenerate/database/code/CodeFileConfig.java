package com.sailing.codegenerate.database.code;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 代码生成的路径配置信息
 * @author YW
 *
 */
public class CodeFileConfig {
	/**
	 * 数据库表名
	 */
	private String tableName;
	/**
	 * 主键ID字段名称
	 */
	private String pkIdFieldName;
	/**
	 * entity类存放的package根路径
	 */
	private String entityPackage;
	/**
	 * entityVO类存放的package根路径
	 */
	private String entityVOPackage;
	/**
	 * dao类存放的package根路径
	 */
	private String repositoryPackage;
	/**
	 * service类存放的package根路径
	 */
	private String servicePackage;
	/**
	 * controller类存放的package根路径
	 */
	private String controllerPackage;
	/**
	 * 查询类存放的package根路径
	 */
	private String queryPackage;
	/**
	 * jsp存放的根路径
	 */
	private String jspPackage;
	/**
	 * 文件作者
	 */
	private String author = "admin";
	/**
	 * 文件创建时间
	 */
	private String createDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
	/**
	 * 版本信息
	 */
	private String version = "v1.0";
	/**
	 * 是否在实体字段中生成swagger-API注解信息
	 */
	private Boolean swaggerApi = false;
	/**
	 * controller头信息中的requestMapping
	 * @RequestMapping(value = "/xxx/xxx")
	 */
	private String requestMapping = "";
	/**
	 * 设置controller文件信息
	 * @param controllerPackage controller生成包根目录
	 * @param requestMapping 访问根路径
	 * @param swaggerApi 设置实体文件中字段是否生成swagger-api注解 true：生成注解|false：不生成注解
	 */
	public void setControllerFile(String controllerPackage, String requestMapping, Boolean swaggerApi) {
		this.controllerPackage = controllerPackage;
		this.requestMapping = requestMapping;
		this.swaggerApi = swaggerApi;
	}
	
	public String getPkIdFieldName() {
		return pkIdFieldName;
	}

	public void setPkIdFieldName(String pkIdFieldName) {
		this.pkIdFieldName = pkIdFieldName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEntityPackage() {
		return entityPackage;
	}

	public void setEntityPackage(String entityPackage) {
		this.entityPackage = entityPackage;
	}

	public String getRepositoryPackage() {
		return repositoryPackage;
	}

	public void setRepositoryPackage(String repositoryPackage) {
		this.repositoryPackage = repositoryPackage;
	}

	public String getServicePackage() {
		return servicePackage;
	}

	public void setServicePackage(String servicePackage) {
		this.servicePackage = servicePackage;
	}

	public String getControllerPackage() {
		return controllerPackage;
	}

	public void setControllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
	}
	
	public String getQueryPackage() {
		return queryPackage;
	}

	public void setQueryPackage(String queryPackage) {
		this.queryPackage = queryPackage;
	}

	public String getJspPackage() {
		return jspPackage;
	}

	public void setJspPackage(String jspPackage) {
		this.jspPackage = jspPackage;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getSwaggerApi() {
		return swaggerApi;
	}

	public void setSwaggerApi(Boolean swaggerApi) {
		this.swaggerApi = swaggerApi;
	}

	public String getRequestMapping() {
		return requestMapping;
	}

	public void setRequestMapping(String requestMapping) {
		this.requestMapping = requestMapping;
	}

	public String getEntityVOPackage() {
		return entityVOPackage;
	}

	public void setEntityVOPackage(String entityVOPackage) {
		this.entityVOPackage = entityVOPackage;
	}
	
}
