package com.sailing.codegenerate.database.utils;

public class StringKits {
	/***
	 * 下划线命名转为驼峰命名
	 * 
	 * @param para
	 *        下划线命名的字符串
	 * @param firstCharToUpcase 第一个字符是否大写 true 返回值第一个字符大写
	 */
    public static String underlineToHump(String para, boolean firstCharToUpcase){
        StringBuilder result=new StringBuilder();
        String a[] = para.split("_");
        for(String s:a){
            if(result.length()==0){
                result.append(s.toLowerCase());
            }else{
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        if(!firstCharToUpcase){
        	return result.toString();
        }
        byte[] items = result.toString().getBytes();  
        items[0] = (byte)((char)items[0] - ( 'a' - 'A'));  
        String rsStr = new String(items);
        return rsStr;
    }
    
    public static String underlineToHump(String para){
        return underlineToHump(para, false);
    }
	/***
	* 驼峰命名转为下划线命名
	 * 
	 * @param para
	 *        驼峰命名的字符串
	 */
    public String humpToUnderline(String para){
        StringBuilder sb=new StringBuilder(para);
        int temp=0;//定位
        for(int i=0;i<para.length();i++){
            if(Character.isUpperCase(para.charAt(i))){
                sb.insert(i+temp, "_");
                temp+=1;
            }
        }
        return sb.toString().toUpperCase(); 
    }
}

