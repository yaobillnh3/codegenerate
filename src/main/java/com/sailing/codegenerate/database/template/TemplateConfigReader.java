package com.sailing.codegenerate.database.template;

import java.util.ResourceBundle;

public class TemplateConfigReader {

	private static TemplateConfigReader reader = new TemplateConfigReader();
	private static ResourceBundle config;

	private TemplateConfigReader() {
		config = ResourceBundle.getBundle("freemarker//freemarker_config");
	}

	public static TemplateConfigReader getInstance() {
		return reader;
	}

	public TemplateConfig readProperties() {
		TemplateConfig templateConfig = new TemplateConfig();
		templateConfig.setProjectDir(config.getString("projectDir"));
		templateConfig.setTemplatePath(config.getString("templatePath"));
		templateConfig.setEncoding(config.getString("encoding"));
		return templateConfig;
	}
}
