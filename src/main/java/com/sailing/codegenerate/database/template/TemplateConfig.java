package com.sailing.codegenerate.database.template;

public class TemplateConfig {
	/**
	 * 工程根路径
	 */
	public String projectDir;
	/**
	 * 模板文件放置根路径
	 */
	public String templatePath;
	/**
	 * 生成文件编码
	 */
	public String encoding = "utf-8";

	public String getProjectDir() {
		return this.projectDir;
	}

	public String setProjectDir(String projectDir) {
		return this.projectDir = projectDir;
	}

	public String getTemplatePath() {
		return TemplateConfig.class.getResource("/").getPath() + templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
