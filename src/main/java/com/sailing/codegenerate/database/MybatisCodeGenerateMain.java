package com.sailing.codegenerate.database;

import com.sailing.codegenerate.database.code.CodeFileConfig;
import com.sailing.codegenerate.database.code.CodeFileWriter;

/**
 * 读取数据库表，生成已mybatis-plugns为数据库框架的业务代码
 *
 * @author YW
 *
 */
public class MybatisCodeGenerateMain {

	public static void main(String[] args) {
		// 生成后台文件
		MybatisCodeGenerateMain.generateFiles();
	}

	/**
	 * 生成后台文件
	 */
	public static void generateFiles() {

		CodeFileConfig codeFileConfig = new CodeFileConfig();
		String modelName = "t_xt_suspicion_number";
		// entity存放package根目录
		codeFileConfig.setEntityPackage("com.sailing.xt.objects.po");
		// VO存放package根目录
		codeFileConfig.setEntityVOPackage("com.sailing.xt.objects.vo");
		// dao存放package根目录
		codeFileConfig.setRepositoryPackage("com.sailing.xt.objects.repository");
		// service存放package根目录
		codeFileConfig.setServicePackage("com.sailing.xt." + modelName + ".service");
		// controller存放package根目录|requestMapping的路径|设置实体文件中字段是否生成swagger-api注解
		codeFileConfig.setControllerFile("com.sailing.xt." + modelName + ".ws", "/" + modelName, true);
		// 数据库表名称
		codeFileConfig.setTableName("t_xt_suspicion_number");
		// 设置表主键ID字段名称
		codeFileConfig.setPkIdFieldName("id");
		// 文件作者
		codeFileConfig.setAuthor("YW");

		CodeFileWriter templateReader = new CodeFileWriter(codeFileConfig);
		try {
			templateReader.init();
			templateReader.generateEntityFiles();
			templateReader.generateDaoFiles();
			templateReader.generateServiceFiles();
			templateReader.generateControllerFiles();
			templateReader.generateEntityVoFiles();
			templateReader.generateEntityBOFiles();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
