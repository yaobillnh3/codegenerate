package com.sailing.codegenerate.excelfile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;

/**
 * 天启V1.1.8及以前版本
 * @ClassName: ImportExcelDataMain
 * @Description: 导出excel中es表结构生成es实体java文件 
 * @Author nyl
 * @DateTime 2020年5月19日 下午1:26:56
 */
public class ExcelDataCodeGenerateMainV118 {
	
	public static void main(String[] args) throws IOException {
		// 生成entity文件路径
		String entityPath = "D:/codegenerate/";
		// excel名称和后缀
		String excelFileName = "青浦-天启数据表结构V1.8.xlsx";
		// excel开始读取行数
		int titleRows = 4;
		// excelsheet索引
		int startSheetIndex = 20;
		// 设置日期格式
		// 生成entity类注释和es索引名称
		ProPerTies propers = new ProPerTies();
		propers.setPackageName("com.sailing.stapsservice.foothold.pojo");
		propers.setIndexName("ads_ljd_clgjmx_dd");
		propers.setClassName("ads_ljd_clgjmx_dd");
		propers.setDescription("车辆历史落脚点明细");
		propers.setAuthor("YW");
		propers.setDateTime(new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒").format(new Date()));
		defaultImport(entityPath, excelFileName, titleRows, startSheetIndex, propers);
	}

	/**
	 * 
	 * @Title: defaultImport
	 * @Description:
	 * @Author nyl
	 * @DateTime 2020年5月18日 下午7:53:23
	 * @param entityPath
	 *            生成entity文件路径
	 * @param excelFileName
	 *            excel名称和后缀
	 * @param titleRows
	 *            excel开始读取行数
	 * @param startSheetIndex
	 *            excelsheet索引
	 * @param propers
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void defaultImport(String entityPath, String excelFileName, int titleRows, int startSheetIndex,
			ProPerTies propers) throws IOException {
		// 换行符
		String lineFeed = "\r\n";
		// Tab键
		String tab = "\t";
		ImportParams importParams = new ImportParams();
		importParams.setStartSheetIndex(startSheetIndex);
		importParams.setTitleRows(titleRows);
		List<?> datas = ExcelImportUtil.importExcel(new File(entityPath + excelFileName), Map.class, importParams);
		try (OutputStream os = new FileOutputStream(new File(entityPath + propers.getClassName() + ".java"));
				OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
				BufferedWriter bw = new BufferedWriter(osw);) {
			// jar引用
			bw.write("package " + propers.getPackageName() + ";" + lineFeed + lineFeed + "import java.sql.Date;" + lineFeed
					+ lineFeed + "import org.springframework.data.annotation.Id;" + lineFeed
					+ "import org.springframework.data.elasticsearch.annotations.Document;" + lineFeed + lineFeed
					+ "import lombok.Data;" + lineFeed);
			// 类注释
			bw.write("/**" + lineFeed + " * " + lineFeed + " * @ClassName: " + propers.getClassName() + lineFeed
					+ " * @Description: " + propers.getDescription() + lineFeed + " * @Author " + propers.getAuthor()
					+ lineFeed + " * @DateTime " + propers.getDateTime() + lineFeed + " */" + lineFeed);
			// 注解引用
			bw.write("@Document(indexName = \"" + propers.getIndexName() + "\", type = \"docs\", createIndex = false)"
					+ lineFeed + "@Data" + lineFeed);
			// 类名称
			bw.write("public class " + propers.getClassName() + " { " + lineFeed);
			for (Object data : datas) {
				Map<Object, Object> map = (Map) data;
				// 字段中文注释
				String bzzwm = "";
				// 字段类型
				String zdlx = "";
				// 字段名称
				String bzzdm = "";
				if (map.containsKey("字段注释")) {
					bzzwm = map.get("字段注释").toString();
				} else if (map.containsKey("标准中文名称")) {
					bzzwm = map.get("标准中文名称").toString();
				} else {
					throw new Exception("无法找到对应字段中文注释！");
				}
				bw.write(tab + "/**" + lineFeed + tab + " * " + bzzwm + lineFeed + tab + " */" + lineFeed);
				if (map.containsKey("字段类型")) {
					zdlx = map.get("字段类型").toString();
				} else {
					throw new Exception("无法找到对应字段类型注释！");
				}
				if (map.containsKey("字段名")) {
					bzzdm = map.get("字段名").toString();
				} else {
					throw new Exception("无法找到对应标准字段名！");
				}
				if ("UUID".equalsIgnoreCase(bzzdm)) {
					bw.write(tab + "@Id" + lineFeed);
				}
				if (zdlx.contains("date")) {
					bw.write(tab + "private Date " + bzzdm + ";" + lineFeed);
				} else if (zdlx.equals("integer")) {
					bw.write(tab + "private Integer " + bzzdm + ";" + lineFeed);
				} else if (zdlx.equals("double")) {
					bw.write(tab + "private Double " + bzzdm + ";" + lineFeed);
				}else {
					bw.write(tab + "private String " + bzzdm + ";" + lineFeed); 
				}
			}
			bw.write("}");
		} catch (Exception e) {
			System.out.println("读取excel失败：" + e);
		}
	}

}
