package com.sailing.codegenerate.excelfile;

/**
 * 
 * @ClassName: properties
 * @Description: 对象属性 
 * @Author nyl
 * @DateTime 2020年5月18日 下午7:03:27
 */
public class ProPerTies {
	/**
	 * 包名称路径
	 */
	private String packageName;
	/**
	 * 类名称
	 */
	private String className;
	/**
	 * 类注释说明
	 */
	private String description;
	/**
	 * 类注释作者
	 */
	private String author;
	/**
	 * 类注释创建时间
	 */
	private String dateTime;
	/**
	 * es索引名称
	 */
	private String indexName;
	
	/**
	 * 包名称路径
	 */
	public String getPackageName() {
		return packageName;
	}
	/**
	 * 包名称路径
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	/**
	 * 类名称
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * 类名称
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * 类注释说明
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 类注释说明
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * 类注释作者
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * 类注释作者
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * 类注释创建时间
	 */
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * 类注释创建时间
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * es索引名称
	 */
	public String getIndexName() {
		return indexName;
	}
	/**
	 * es索引名称
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

}
