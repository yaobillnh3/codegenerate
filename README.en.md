# codegenerate

#### 介绍
项目代码生成器-1、适用于生成mybatis-plugs为dao组件的PO/mapper/service/controller;2、将excel文档里面的表描述生成为java实体。

#### 软件架构
软件架构说明

com.sailing.codegenerate
                   .database 根据数据库表生成mybatis的po/vo/mapper/service/controller文件 
                   .excelfile 读取excel的文件中表的结构定义，生成es实体文件（后期可考虑生成dao等文件）

#### 使用说明

1. 生成mybatis的po/vo/mapper/service/controller文件
   1.1配置application-datasource.properties里面数据源信息。
   1.2配置MybatisCodeGenerateMain.java表和主键、作者等信息。
   1.3运行MybatisCodeGenerateMain.java
2. 成elasticseach的实体文件。
   2.1配置并运行ExcelDataCodeGenerateMain.java

#### 参与贡献

1.  yaobillnh3 
2.  nyl688
