#!/bin/sh

####################################################################################################
#
# 该脚本用于Linux系统下备份PostgreSQL数据
# 采用pg_dump工具导出SQL文件，并压缩存放
# 备份后删除保留天数以前的历史备份文件
# 
#
# 备份示例：
# 执行
# ./backup-postgresql.sh
#
#
# 添加系统定时任务
# # crontab -e
# 追加如下内容保存并退出（每天凌晨1点开始备份）
# 0 1 * * * sh /workspace/scripts/backup-postgresql.sh
# 
#
# 恢复示例：
# 1. 解压数据库备份文件
# unzip mydb-20211203142616.bak.zip
#
# 2. 创建数据库
# PostgreSQL命令行创建数据库（数据库名需与待恢复的数据库名一致）
# postgres=# create database mydb with owner mydbdata tablespace tbs_mydb;
#
# 3. 导入数据
# 切换到postgres用户
# su - postgres
# 通过psql工具导入数据
# psql -h 192.168.56.100 -p 5432 -d mydb -U postgres < mydb-20211203142616.bak
# 
#
# 注意事项：
# - 保证执行备份脚本的用户具备备份文件存放目录的读写权限
#
#gunzip -c mingjing_dev-20240529143406.bak.zip | /opt/component/postgreSql/bin/psql -h 172.20.52.100 -U mingjing -p 55432 -d mingjing_dev
#恢复数据库
#/opt/component/postgreSql/bin/psql -h 172.20.52.100 -U mingjing -p 55432 -d mingjing_dev -f "mingjing_dev-20240529143406.bak"
#创建数据库用户
#CREATE USER mingjing_dev WITH PASSWORD 'Sailing@123';
####################################################################################################

##################################################
# 环境变量及系统参数
##################################################
# PostgreSQL安装目录
PGHOME=/opt/component/postgreSql
# Host
PGHOST=192.168.1.100
# 监听端口
PGPORT=55432
# 用户
PGUSER=mingjing_dev
# 密码
PGPASSWORD=Sailing@123
# 数据库
DBNAME=mingjing_dev
# 编码
ENCODING=UTF8

# 备份文件存放目录
BACKUP_DIR=/opt/db-data-backup/data
# 备份文件名称
BACKUP_FILE_NAME=${1}
# 备份文件保留天数，默认2天
BACKUP_FILE_RESERVED_DAYS=2

# 脚本名称
SCRIPT_NAME="$(basename "${0}")"
# 备份执行日志存放目录
BACKUP_LOG_PATH=/opt/db-data-backup/log
# 备份执行日志文件名称
BACKUP_LOG_FILENAME=${BACKUP_LOG_PATH}/${SCRIPT_NAME}-$(date +%Y%m%d%H%M%S).log


# 准备
function prepare() {
    if [ ! -d "${BACKUP_DIR}" ]; then
        mkdir -p "${BACKUP_DIR}"
    fi
    if [ ! -d "${BACKUP_LOG_PATH}" ]; then
        mkdir -p "${BACKUP_LOG_PATH}"
    fi
    if [ ! -f "${BACKUP_LOG_FILENAME}" ]; then
        touch "${BACKUP_LOG_FILENAME}"
    fi
}

# 记录INFO日志
function info() {
  echo "$(date "+%Y-%m-%d %H:%M:%S") [INFO] ${1}" >> "${BACKUP_LOG_FILENAME}"
}

# 记录ERROR日志
function error() {
  echo -e "$(date "+%Y-%m-%d %H:%M:%S") \033[31m[ERROR]\033[0m ${1}" >> "${BACKUP_LOG_FILENAME}"
}

# 恢复备份数据
function restore() {
    info "备份数据库${DBNAME}开始"
    export PGPASSWORD
	gunzip -c mingjing_dev-20240529143406.bak.zip | ${PGHOME}/bin/psql -h ${PGHOST} -U ${PGUSER} -p ${PGPORT} -d ${DBNAME}  >> ${BACKUP_LOG_FILENAME} 2>&1 
 # 获取上一个命令的退出状态(0表示正常退出)
 restore_status=$?
    if [ ! ${restore_status} -eq 0 ]; then
        error "恢复数据库${DBNAME}失败"
        exit 1
    fi
    info "数据库${DBNAME}恢复文件：${BACKUP_DIR}/${BACKUP_FILE_NAME}"
    info "备份数据库${DBNAME}结束"
}



function doRestore() {
    restore
}

doRestore

exit 0

